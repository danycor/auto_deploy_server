# auto_deploy_server

Script to auto deploy a application on a server

## Installation

### On the server
* `adduser AUTODEPLOY_USER`
* `su root`
* `cd /YOUR_DIR_PATH`
* `git clone https://gitlab.com/danycor/auto_deploy_server.git`

#### AND
* `cd auto_deploy_server`
* `chmod +x ./install.bash`
* `./install AUTODEPLOY_USER`

#### OR MANUAL INSTALL

* `chown AUTODEPLOY_USER:AUTODEPLOY_USER ./auto_deploy_server/autodeploy.bash`
* `chmod +x ./auto_deploy_server/autodeploy.bash`
* `cp -s /YOUR_DIR_PATH/auto_deploy_server/autodeploy.bash /home/AUTODEPLOY_USER/bin/autodeploy`
* `chown AUTODEPLOY_USER:AUTODEPLOY_USER /home/AUTODEPLOY_USER/bin/autodeploy`
* `source ~/.profile`
* `source ~/.bashrc`

### On gitlab
#### Variables
Go to :
* Gitlab
    * Your project
        * Setting
            * CI / CD
                * Variables

Enter all variables describe in Args section.

#### .gitlab-ci.yml
Add to your project a .gitlab-ci.yml and link it to the autodeploy.

.gitlab-ci.yml exemple is in .gitlab-ci.exemple.yml

Change the file to yout needs.

## Args
|VarName|Desc|
|---|---|
|USER|User name on server to exec autodeploy script|
|USER_PASS|User server password|
|SERVER|The server URL/DNS/IP (dany-corbineau.fr)|
|IMAGE_NAME|The image name stored on the hub|
|DOCKER_SERVER_HOST|The docker server registry (dany-corbineau.fr:5001)|
|DOCKER_USER|User name Docker registry|
|DOCKER_PASS|Docker password|
|COMPOSER_PATH|Composer file path on server (/etc/project/docker-compose.yml)|
|SSH_PORT|The ssh port of the sever (22)|

## Update auto_deploy :
* `cd /YOUR_DIR_PATH/auto_deploy_server`
* `git pull origin master`