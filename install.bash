#!/bin/bash
chown $1:$1 autodeploy.bash
chmod +x autodeploy.bash
cp -s $PWD/autodeploy.bash /home/$1/bin/autodeploy
chown $1:$1 /home/$1/bin/autodeploy
source ~/.profile
source ~/.bashrc