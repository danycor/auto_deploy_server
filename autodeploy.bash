#!/bin/bash

VERSION="0.1"

if [ $# -lt 1 ]; then
    echo "Version : $VERSION"
fi

if [ $# -gt 4 ]; then 

docker login -u$1 -p$2 $5
docker pull $5/$1/$3:latest
docker-compose -f $4 down
docker-compose -f $4 up -d
echo "$(date) : USER : $1 AUTODEPLOY $5/$1/$3 IN $4;" > run.log

else
    echo "Need 5 args : docker_user docker_password image_name path/docker-compose.yml docker_hub_host" 
fi


exit 0